#BASE
FROM php:7.4-apache

#COPY SOURCE CODE
COPY . /var/www/html/

#CREATE UPLOAD FOLDER
RUN mkdir /var/www/html/uploads

#INSTALL MYSQL EXTENSION
RUN docker-php-ext-install mysqli

#ENABLE MPM WORKER
RUN a2enmod mpm_prefork

#HEALTHCHECK
HEALTHCHECK --interval=1m --timeout=5s --retries=3 \
  CMD curl --fail http://localhost:80 || exit 1
